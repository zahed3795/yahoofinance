package runner;


import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


	@RunWith(Cucumber.class)
	@CucumberOptions(
        features = {//"C:\\Users\\zahed\\Desktop\\freamwork\\framework\\src\\test\\java\\features\\signUp.feature",
        			"C:\\Users\\zahed\\Desktop\\freamwork\\framework\\src\\test\\java\\features\\login.feature"
        },
     
        glue = {"robinhood.com", },
        //dryRun = true,
        plugin = {"pretty:target/cucumber-test-report/cucumber-pretty.txt",
         },
        format= {"pretty","html:test-outout", "json:json_output/cucumber.json", "junit:junit_xml/cucumber.xml"}//to generate different types of reporting

)

public class BDD_JUnit {
}
